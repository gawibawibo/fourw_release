# FourW_release
## beta/1.1.0  
### 수정 사항
- Search 검색 기능 추가
- UI 수정
- Google Form 의견 작성 기능 추가

### 유의 사항
beta/1.0.0 또는 beta/1.0.1 이 설치되어 있을 경우, 기존 앱을 삭제 후 beta/1.0.2 를 설치해 주시기 바랍니다.

# FourW_release
## beta/1.2.1  
### 새로운 기능
  
### 수정 사항
- style detail 화면에서 이미지가 안보이는 오류 수정
- style detail 화면에서 새로고침 시 날씨 데이터가 정상적으로 업데이트 되지 않는 오류 수정

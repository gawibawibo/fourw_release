# FourW  
## Four W : 언제(When), 어디서(Where), 무엇을(What), 입었는지(Wearing)  
- 언제, 어디서, 무엇을 입었는지 기록하고, 날씨 별 옷 기록을 검색할 수 있습니다.  
- 급격한 날씨 변화, 타 지역으로 이동·여행 시, 날씨에 따른 적절한 옷차림을 기록하고 확인할 수 있습니다.  
![fourw main](./resource/FourW Readme.png) 

## apk 파일 설치 방법
apk 파일 다운로드  
→   apk 파일을 핸드폰 내부 자신이 기억할 수 있는 경로에 위치시킴  
→   핸드폰에서 apk 파일 클릭  
→   설치 (단말기 별 보안 상의 이유로 설치 의사를 확인하는 알림창이 뜰 수 있습니다.)  

## Google Play
아직 Google Play에 배포하지 않았으며 추후 배포할 예정입니다.  

## 프로젝트 소스 링크
[Four W 프로젝트 소스 링크](https://gitlab.com/gawibawibo/iwim)  
간혹 개발 과정에서 공개 범위가 private 으로 설정되어 프로젝트 열람이 불가능 할 수 있습니다. 참고 바랍니다.